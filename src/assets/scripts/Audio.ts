/**
 * @file Web Audio APIを利用するクラス
 */

export class Audio {
  private static context: AudioContext;
  private buffer: AudioBuffer;
  private source: AudioBufferSourceNode;

  constructor(filePath: string) {
    if (!Audio.context) {
      const AudioContext = (window as any).AudioContext || (window as any).webkitAudioContext;
      Audio.context = new AudioContext();
    }

    this.initAudio(filePath);
  }

  private initAudio(filePath: string): void {
    const request = new XMLHttpRequest();
    request.responseType = 'arraybuffer';

    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        if (request.status === 0 || request.status === 200) {
          Audio.context.decodeAudioData(request.response, (buffer) => {
            this.buffer = buffer;
          });
        }
      }
    }

    request.open('GET', filePath, true);
    request.send('');
  }

  public play(): void {
    if (!this.buffer) return;

    this.source = Audio.context.createBufferSource();
    this.source.buffer = this.buffer;
    this.source.connect(Audio.context.destination);
    this.source.start(0);
  }

  public pause(): void {
    if (!this.source) return;

    this.source.stop(0);
  }

  public close(): void {
    Audio.context.close();
  }
}
