import { resolve } from 'path';

module.exports = {
  css: [
    "reset.css",
    "~/assets/styles/main.styl"
  ],
  head: {
    meta: [
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' }
    ],
    link: [
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Montserrat:200,400' }
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '~/modules/typescript.js'
  ],
  spa: true,
  srcDir: 'src/'
};
