/**
 * @file OpenWeatherMap関連のストア
 */

const APPID = 'a9c3a37dce9c961519a05254648f653d';

export interface IState {
  city: object,
  postalCode: string;
  list: object[];
}

/** OpenStreetMap関連のストア */
export const state = () => ({
  city: {},
  postalCode: '',
  list: []
}) as IState;

/** OpenStreetMap関連のミューテーション */
export const mutations = {
  /** 郵便番号を設定する */
  setPostalCode(state: IState, postalCode: string): void {
    state.postalCode = postalCode;
  },

  /** 天気予報を設定する */
  setWeatherList(state: IState, list: object[]) {
    state.list = list;
  },

  /** 都市情報を設定する */
  setCity(state: IState, city: object) {
    state.city = city;
  }
};

/** OpenStreetMap関連のアクション */
export const actions = {
  /** 郵便番号から天気を取得 */
  async fetchWeather({ commit }, postalCode: string): Promise<object> {
    const { city, list } = await this.$axios.$get('/forecast', {
      params: {
        APPID,
        zip: `${postalCode.replace(/^(\d{3})(\d{4})/, '$1-$2')},JP`
      }
    });

    commit('setPostalCode', postalCode);
    commit('setWeatherList', list);
    commit('setCity', city);

    return { city, list };
  }
}
